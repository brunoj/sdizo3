//
// Created by brunoj on 01.06.17.
//

#include "Graph.h"
#include <iostream>
#include <iomanip>

using namespace std;

Graph::Graph():V(0), E(0), poczatek(0), koniec(0) { }
Graph::~Graph() {
    for(int i = 0; i < getV(); i++) delete[] G[i];
    delete[] G;
}
void Graph::wyswietl() {
    // Jesli graf nie zawiera wierzcholkow
    if(getV() == 0) {
        cout << "Nie ma czego wyswietlic." << endl;
        return;
    }

    cout << "Postac macierzowa grafu: " << endl << endl;
    // Wyswietlenie wagi krawedzi wychodzacej z wierzcholka 'i' do wierzcholka 'j'nie m
    for(int i = 0; i < getV(); i++) {
        for(int j = 0; j < getV(); j++) {
            cout << setw(4) << G[i][j] << " ";
        }
        cout << endl << endl;
    }
}
void Graph::generuj() {
    int ilosc_wierzcholkow, gestosc, krawedzie, maksymalne_ilosc_krawedzi;
    cout << "Podaj liczbe wierzcholkow: ";
    cin >> ilosc_wierzcholkow;

    // Jezeli podalismy za malo wierzcholkow
    if (ilosc_wierzcholkow < 2) {
        cout << "Graf musi posiadac co najmniej 2 miasta!" << endl;
        return;
    }

 gestosc=100;


    // Obliczanie maksymalnej liczby krawedzi oraz zadanej liczby krawedzi (gestosc)
    maksymalne_ilosc_krawedzi = ilosc_wierzcholkow * ilosc_wierzcholkow - ilosc_wierzcholkow;
    krawedzie = 0.01 * gestosc * maksymalne_ilosc_krawedzi;





    if (getV() != 0) {
        for (int i = 0; i < getV(); i++) delete[] G[i];
        delete[]  G;

    }
    setV(ilosc_wierzcholkow);
    setE(krawedzie);
    G = new int *[getV()];
    for (int i = 0; i < getV(); i++) G[i] = new int[getV()];

    // Wyzerowanie wszystkich krawedzi
    for (int i = 0; i < getV(); i++) { for (int j = 0; j < getV(); j++) G[i][j] = 0; }

    int spojnosc = 0, wiersz, kolumna;
    bool polaczone[ilosc_wierzcholkow], graf_spojny = false;
    for (int i = 0; i < ilosc_wierzcholkow; i++) polaczone[i] = false;
    int waga;
    waga = (rand() % 9) + 1;
    wiersz = 0;
    setPoczatek(0);

    kolumna = rand() % (ilosc_wierzcholkow - 1) + 1;

    G[wiersz][kolumna] = waga;

    polaczone[0] = true;
    polaczone[kolumna] = true;

    G[kolumna][wiersz] = G[wiersz][kolumna];


    while (!graf_spojny) {
        spojnosc = 0;
        for (int i = 0; i < ilosc_wierzcholkow; i++) if (polaczone[i] == true) spojnosc++;

        if (spojnosc == ilosc_wierzcholkow) break;

        wiersz = kolumna;
        while (wiersz == kolumna || polaczone[kolumna] == true) kolumna = rand() % ilosc_wierzcholkow;

        waga = (rand() % 9) + 1;
        G[wiersz][kolumna] = waga;
        G[kolumna][wiersz] = G[wiersz][kolumna];

        ;

        polaczone[kolumna] = true;


    }


    // Ustawienie wierzcholka koncowego

    setKoniec(kolumna);

    // Tak wygenerowany graf na pewno jest spojny
    graf_spojny = true;

    // Ustalenie ile jeszcze krawedzi mozna dolozyc do grafu (do_konca)
    int do_konca = 0;
    do_konca = krawedzie - (2 * (spojnosc - 1));


    // Dokladanie kolejnych krawedzi tak dlugo, az spelnimy warunek gestosci
    while (do_konca > 0 && graf_spojny) {
        // Losowanie wiersza i kolumny / sasiada tak dlugo, az bedziemy pewni ze dodamy nowe polaczenie
        while ((G[wiersz][kolumna] != 0) || (wiersz == kolumna)) {
            wiersz = rand() % ilosc_wierzcholkow;
            kolumna = rand() % ilosc_wierzcholkow;
        }

        // Losowo dobrana waga krawedzi
        waga = (rand() % 9) + 1;

        // Ustanowienie krawedzi w postaci macierzowej i listowej
        G[wiersz][kolumna] = waga;

        // Dodatkowy warunek (krawedz w druga strone) jesli graf jest nieskierowany

        G[kolumna][wiersz] = waga;



        // Zmniejszenie ilosci pozostalych do dodania krawedzi
        do_konca -= 2;

    }
   //2
    //modyfikowanie do eulera
//
}
void Graph::wczytaj(std::string nazwa) {
        ifstream plik;
        plik.open(nazwa);

        if(!plik.good()) {
            cout << "Nie ma takiego pliku." << endl;
            return;
        }

        int licznik = 0, argument;
        string zawartosc_wiersza, pomoc = "";
        getline(plik, zawartosc_wiersza);

        if(getV() != 0) {
            for(int i = 0; i < getV(); i++) delete[] G[i];
            delete[] G;
        }

        for(int i = 0; i < 4; i++) {
            while(zawartosc_wiersza[licznik] < 58 && zawartosc_wiersza[licznik] > 47) {
                pomoc += zawartosc_wiersza[licznik];
                licznik++;
            }

            argument = atoi(pomoc.c_str());
            if(i == 0) { setE(argument); }
            if(i == 1) { setV(argument); }
            if(i == 2) { setPoczatek(argument); }
            if(i == 3) { setKoniec(argument); }

            licznik++;
            pomoc = "";
        }

        G = new int*[getV()];
        for(int i = 0; i < getV(); i++) G[i] = new int[getV()];
        for(int i = 0; i < getV(); i++) { for(int j = 0; j < getV(); j++) G[i][j] = 0; }

        licznik = 0;
        int masymalna_ilosc_krawedzi = getV() * getV() - getV();
        int wiersz, kolumna, waga;
        for(int i = 0; i < masymalna_ilosc_krawedzi; i++) {
            getline(plik, zawartosc_wiersza);
            if(zawartosc_wiersza[0] > 57 || zawartosc_wiersza[0] < 48 || i == getE()) break;

            for(int j = 0; j < 3; j++) {
                while(zawartosc_wiersza[licznik] < 58 && zawartosc_wiersza[licznik] > 47 || zawartosc_wiersza[licznik] == 45) {
                    pomoc += zawartosc_wiersza[licznik];
                    licznik++;
                }

                argument = atoi(pomoc.c_str());
                if(j == 0) wiersz = argument;
                if(j == 1) kolumna = argument;
                if(j == 2) waga = argument;

                licznik++;
                pomoc = "";
            }

            licznik = 0;
            G[wiersz][kolumna] = waga;
            G[kolumna][wiersz] = waga;
        }

        plik.close();

}