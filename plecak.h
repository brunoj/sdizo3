//
// Created by brunoj on 02.06.17.
//

#ifndef SDIZO3_PLECAK_H
#define SDIZO3_PLECAK_H
#include"item.h"
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <ctime>
#include <vector>

using namespace std;

class plecak {
public:
    int iloscPrzedmiotow = 0;
    int rozmiarPlecaka;
    int maxWaga;
    int obecnaWaga;
    int obecnaWartosc;
    vector<item>wPlecaku;
    vector<item>przedmioty;

    plecak() {

    }

    plecak(int a, int w) {
        iloscPrzedmiotow = a;
        maxWaga = w;
    }
    void wczytaj(plecak *p, std::string name);
    void losuj() {

        int maxWagaL = 20;
        int maxWartoscL = 30;

        przedmioty.clear();

        for (int i = 0; i < iloscPrzedmiotow; i++) {

            item tmp(rand() % maxWagaL + 1, rand() % maxWartoscL + 1);
            przedmioty.push_back(tmp);
        }

    }

    void wypisz();

    void bruteForce();



    void greedWartosc() ;

    void greedStosunek() ;

    void wypiszP() {
        cout << endl << endl << "\nZawaroasc Plecaka:\n\nwaga\twartosc\tstosunek\n\n";
        for (int i = 0; i < wPlecaku.size(); i++) {


            cout << wPlecaku.at(i).waga << "\t " << wPlecaku.at(i).wartosc << "\t " << wPlecaku.at(i).v << endl;

        }

        cout << endl << endl << "wartos: " << obecnaWartosc << "  waga:" << obecnaWaga << endl;
    }

};


#endif //SDIZO3_PLECAK_H
