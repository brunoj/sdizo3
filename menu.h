//
// Created by brunoj on 30.05.17.
//

#ifndef SDIZO3_MENU_H
#define SDIZO3_MENU_H
#include <iostream>
#include "Graph.h"
#include "plecak.h"

class menu {
private:
    plecak *p;
    void komiwojazer();
    Graph *graph;
public:
    void plecakmenu();
    menu();
    void menuGlowne();
    void okno_startowe();

};


#endif //SDIZO3_MENU_H
