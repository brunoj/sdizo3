//
// Created by brunoj on 30.05.17.
//


#include "menu.h"
#include "Graph.h"
#include "salesman.h"
#include "plecak.h"
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <ctime>
#include <vector>


using namespace std;



menu::menu(){
    graph=new Graph();
    okno_startowe();
    menuGlowne();
}



void menu::okno_startowe() {
    cout << "::                   SDIZO PROJEKT 3                     ::" << endl;
    cout << "::                                                       ::" << endl;
    cout << "::                  Bruno Jamuła 210370                   ::" << endl;


}

void menu::menuGlowne() {
    int wybor;
    cout << "1 ---- Problem Plecakowy" << endl;
    cout << "2 ---- Komiwojazer" << endl;
    cin >> wybor;
    switch (wybor) {
        case 1:
            plecakmenu();
            break;
        case 2:
            komiwojazer();
            break;
        default:
            cout << "nie ma takiego wyboru" << endl;
            menuGlowne();
    }
}


void menu::plecakmenu(){

    int wybor;
    cout << "1 ---- Wczytaj z Pliku" << endl;
    cout << "2 ---- Wygeneruj losowo" << endl;

    cin >> wybor;
    int a, b;

    int wybor2;
    bool wyjsc=false;
    p = new plecak();

    switch (wybor) {
        case 1:

        case 2:
            cout << "Podaj liczbe przedmiotow: ";
            cin >> a;

            cout << "Podaj pojemnosc plecaka: ";
            cin >> b;
            p = new plecak(a, b);
            p->losuj();
            while(wyjsc!=true){
                cout << "1 --- wyswietl plecak" << endl;
                cout << "2 --- wyswietl przedmioty"<<endl;
                cout << "3 --- brut force" << endl;
        //        cout << "4 --- zachlanny wartosci" << endl; brak testow
                //      cout << "5 --- zachlanny stosunek" << endl; brak testow
                cin>>wybor2;

                switch (wybor2) {
                    case 1:
                        p->wypiszP();
                        break;
                    case 2:
                        p->wypisz();
                        break;
                    case 3:
                        cout << endl << "Przeglad zupelny ";
                        p->bruteForce();
                        p->wypiszP();
                        break;
                    case 4:
                        cout << endl << "Chciwosci wartosci ";
                        p->greedWartosc();
                        p->wypiszP();

                        break;
                    case 5:

                        cout << endl << "Chciwosci stosunek ";
                        p->greedStosunek();
                        p->wypiszP();
                        break;
                    default:
                        wyjsc = true;
                        break;
                }
        }




        case 7:
            menuGlowne();
            break;
        default:
            plecak();
            break;

    }
}

void menu::komiwojazer(){
    salesman salesman(graph);
    int wybor;
    cout<<  "1 ---- Wczytaj z Pliku"    <<endl;
    cout<<  "2 ---- Wygeneruj losowo"   <<endl;
    cout<<  "3 ---- Wyswietl graf"      <<endl;
    cout<<  "4 ---- Brut Force"      <<endl;
    cout<<  "5 ---- Algorytm zachlanny"      <<endl;
    cout<<  "7 ---- Powrot do menu" <<endl;
    cin>>wybor;
    switch (wybor){
        case 1:

            break;
        case 2:
            graph->generuj();
            komiwojazer();
            break;
        case 3:
            graph->wyswietl();
            komiwojazer();
            break;
        case 4:
            salesman.BrutForce();
            komiwojazer();
            break;
        case 5:
            salesman.greedy();
            komiwojazer();
            break;
        case 7:
            menuGlowne();
            break;
        default:
            komiwojazer();
            break;

    }
}