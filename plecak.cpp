//
// Created by brunoj on 02.06.17.
//

#include "plecak.h"
#include <cmath>
#include <climits>
#include <climits>
#include <chrono>
#include <fstream>

typedef std::chrono::high_resolution_clock Clock;

void plecak::wypisz() {
    cout << "Przedmioty:" << endl << endl << "waga\twartosci\tstosunek\n\n";
    for (int i = 0; i < iloscPrzedmiotow; i++) {


        cout << przedmioty.at(i).waga << "\t " << przedmioty.at(i).wartosc << "\t \t" << przedmioty.at(i).v << endl;
    }
}

void plecak::bruteForce() {
    auto t1 = Clock::now();

    vector <int> A;
    int k = 0;
    obecnaWaga = 0;
    obecnaWartosc = 0;
    wPlecaku.clear();

    for (int i = 1; i < pow(2.00, iloscPrzedmiotow); i++) {
        k = i;
        int t = 0;
        int tmpWaga = 0;
        int tmpWartosc = 0;
        while (k)
        {
            A.push_back(k % 2);
            k /= 2;
        }

        for (int j = 0; j < A.size(); j++) {

            if (A.at(j) == 1) {
                tmpWaga = tmpWaga + przedmioty.at(j).waga;
                tmpWartosc = tmpWartosc + przedmioty.at(j).wartosc;
            }
        }

        if (tmpWaga <= maxWaga && tmpWartosc > obecnaWartosc) {
            obecnaWaga = tmpWaga;
            obecnaWartosc = tmpWartosc;
            wPlecaku.clear();
            for (int j = 0; j < A.size(); j++) {

                if (A.at(j) == 1) {
                    wPlecaku.push_back((przedmioty.at(j)));
                    //wPlecaku.push_back(przedmioty.at(j))
                }
            }

        }

        A.clear();
    }
    auto t2 = Clock::now();
    double time = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();
    cout << "Czas wykonania: " << time << endl;

}
void plecak::wczytaj(plecak *p, std::string nazwa) {
    int rozmiar;
    int wartosc;
    ifstream plik;
    plik.open(nazwa);

    if(!plik.good()) {
        cout << "Nie ma takiego pliku." << endl;
        return;
    }

    int licznik = 0, argument;
    string zawartosc_wiersza, pomoc = "";
    getline(plik, zawartosc_wiersza);

    if(iloscPrzedmiotow!=0) {
        for (int i = 0; i < iloscPrzedmiotow; i++) {
            przedmioty.clear();
            wPlecaku.clear();
        }
    }
    p=new plecak();

    for(int i = 0; i < 2; i++) {
        while(zawartosc_wiersza[licznik] < 58 && zawartosc_wiersza[licznik] > 47) {
            pomoc += zawartosc_wiersza[licznik];
            licznik++;
        }
        int pojemnosc;
        int przedmiotypomoc;
        argument = atoi(pomoc.c_str());
        if(i == 0) {  p->maxWaga=pojemnosc;}
        if(i == 1) { p->iloscPrzedmiotow=przedmiotypomoc; }




        licznik++;
        pomoc = "";
    }
    for(int i=0;i<iloscPrzedmiotow;i++){
        getline(plik, zawartosc_wiersza);
        for(int j = 0; j < 2; j++) {
            while(zawartosc_wiersza[licznik] < 58 && zawartosc_wiersza[licznik] > 47 || zawartosc_wiersza[licznik] == 45) {
                pomoc += zawartosc_wiersza[licznik];
                licznik++;
            }

            argument = atoi(pomoc.c_str());
            if(j == 0) rozmiar = argument;
            if(j == 1) wartosc = argument;


            licznik++;
            pomoc = "";
        }
        item tmp(rozmiar,wartosc);
        przedmioty.push_back(tmp);

        licznik = 0;
    }
    plik.close();
}
void plecak::greedWartosc() {
    auto t1 = Clock::now();

    obecnaWaga = 0;
    obecnaWartosc = 0;
    wPlecaku.clear();


    for (int i = 0; i < iloscPrzedmiotow - 1; i++) {
        for (int j = i + 1; j < iloscPrzedmiotow; j++) {
            if (przedmioty.at(i).wartosc < przedmioty.at(j).wartosc) {
                item tmp = przedmioty.at(i);
                przedmioty.at(i) = przedmioty.at(j);
                przedmioty.at(j) = tmp;
            }
        }
    }

    for (int i = 0; i < iloscPrzedmiotow; i++) {
        if (obecnaWaga + przedmioty.at(i).waga <= maxWaga) {
            wPlecaku.push_back(przedmioty.at(i));
            obecnaWaga = obecnaWaga + przedmioty.at(i).waga;
            obecnaWartosc = obecnaWartosc + przedmioty.at(i).wartosc;
        }
    }
    auto t2 = Clock::now();
    double time = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();
    cout << "Czas wykonania: " << time << endl;
}
void plecak::greedStosunek() {
    auto t1 = Clock::now();

    obecnaWaga = 0;
    obecnaWartosc = 0;
    wPlecaku.clear();

    for (int i = 0; i < iloscPrzedmiotow - 1; i++) {
        for (int j = i + 1; j < iloscPrzedmiotow; j++) {
            if (przedmioty.at(i).v < przedmioty.at(j).v) {
                item tmp = przedmioty.at(i);
                przedmioty.at(i) = przedmioty.at(j);
                przedmioty.at(j) = tmp;
            }
        }
    }

    for (int i = 0; i < iloscPrzedmiotow; i++) {
        if (obecnaWaga + przedmioty.at(i).waga <= maxWaga) {
            wPlecaku.push_back(przedmioty.at(i));
            obecnaWaga = obecnaWaga + przedmioty.at(i).waga;
            obecnaWartosc = obecnaWartosc + przedmioty.at(i).wartosc;
        }
    }
    auto t2 = Clock::now();
    double time = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();
    cout << "Czas wykonania: " << time << endl;
}