//
// Created by brunoj on 31.05.17.
//

#ifndef SDIZO3_SALESMAN_H
#define SDIZO3_SALESMAN_H


#include <climits>
#include "Graph.h"

class salesman {
private:
    Graph *graph;
    int *droga;
    int *wk;
    int *v;
    int cost=INT_MAX;
public:
    void greedy();
    salesman(Graph *graph);
     void BrutForce();
    void swap(int *x,int *y);
    void calculate(int *vertex,int n);
    void permute(int*,int,int);
};


#endif //SDIZO3_SALESMAN_H
