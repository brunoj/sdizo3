//
// Created by brunoj on 01.06.17.
//

#ifndef SDIZO3_GRAPH_H
#define SDIZO3_GRAPH_H
#include<iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <iomanip>
using namespace std;
class Graph {
private:
    int V, E;                                                               // V - ilosc wierzcholkow, E - ilosc krawedzi
    int poczatek, koniec;

public:
    Graph();
    ~Graph();
    int** G;                                                                // Tablica wskaznikow przechowujaca wagi krawedzi miedzy wierzcholkami

    void setV(int V) { this -> V = V; };
    void setE(int E) { this -> E = E; };
    void setPoczatek(int poczatek) {this -> poczatek = poczatek; }
    void setKoniec(int koniec) { this -> koniec = koniec; }
    int getV() { return V; }
    int getE() { return E; }
    int getPoczatek() { return poczatek; }
    int getKoniec() { return koniec; }

    void wyswietl();        // Funkcja wyswietlajaca graf w postaci macierzowej
    void generuj();
    void wczytaj(std::string a);




};


#endif //SDIZO3_GRAPH_H
