//
// Created by brunoj on 31.05.17.
//

#include <iostream>
#include "salesman.h"
#include <cmath>
#include <climits>
#include <chrono>
typedef std::chrono::high_resolution_clock Clock;

salesman::salesman(Graph *graph) {
    this->graph=graph;
    droga=new int[graph->getV()+1];

}
void salesman::swap(int *x,int *y){
    int tmp;
    tmp=*x;
    *x=*y;
    *y=tmp;
}
void salesman::calculate(int *vertex,int n){
    int i,sum=0;
    for (i=0;i<=n;i++){
        sum+=graph->G[vertex[i%(n+1)]][vertex[(i+1)%(n+1)]];
    }
    if(cost>sum){
        for (i=0;i<=n;i++){
            droga[i]=vertex[i];
        }
        droga[n+1]=vertex[0];
        cost=sum;

    }
}
void salesman::permute(int *vertex,int i, int n){
    int j,k;

        for(j=i;j<=n;j++){
            calculate(vertex,n);
            swap((vertex+i),(vertex+j));
            permute(vertex,i+1,n);
            swap((vertex+i),(vertex+j));
        }

}
void salesman::BrutForce() {
    auto t1 = Clock::now();

    int vertex[graph->getV()];

    int n=graph->getV();
    for(int i=0;i<n;i++){
        vertex[i]=i;

    }
    permute(vertex,0,n-1);

    std::cout<<std::endl;
    n=n-1;
    int i;
    std::cout<<"droga:  ";
    for (i=0;i<=n+1;i++){


        std::cout<<droga[i];
        if(i!=n+1)
            std::cout<<"-->";
    }
    std::cout<<std::endl<<"minimum cost = "<<cost<<std::endl;
    auto t2 = Clock::now();
    double time = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();
    cout << "Czas wykonania: " << time << endl;
    std::cout<<std::endl;
}

void salesman::greedy(){
    auto t1 = Clock::now();


    int tmpIndex=0;
    int wStartowy = 0;
    int k;
    bool *odwiedzone;
    int ilosc_wierzcholkow = graph->getV();
    odwiedzone = new bool[ilosc_wierzcholkow];


    droga = new int[ilosc_wierzcholkow];
    wk = new int[ilosc_wierzcholkow];
    v = new int[ilosc_wierzcholkow];

    for (int i = 0; i < ilosc_wierzcholkow;i++) {

        odwiedzone[i] = 0;

    }


    odwiedzone[wStartowy] = 1;
    k = wStartowy;
    for (int j = 0; j < ilosc_wierzcholkow-1; j++) {
        int tmpWaga = 9999999;
        //szukanie najtañszej drogi w wierszu
        for (int i = 0; i < ilosc_wierzcholkow; i++) {

            if (graph->G[k][i] < tmpWaga && odwiedzone[i]==0) { //jesli nowa dorga w wierszu tañsza i wierzcho³ek jeszcze
                // nieodwiedzony zast¹p stara
                tmpWaga = graph->G[k][i];
                tmpIndex = i;

            }
        }

        // zapisanie warotsci wiercho³ka pocz¹tkowego,koncowego,wagi
        droga[j] = k;
        wk[j] = tmpIndex;
        v[j] = graph->G[k][tmpIndex];

        k = tmpIndex;
        odwiedzone[k] = 1; // oznacz wierzcho³ek jako odwiedzony

    }

    //po³¹czenie ostatniego wierzcho³ak z wierzcho³kiem startowym
    droga[ilosc_wierzcholkow - 1] = k;
    wk[ilosc_wierzcholkow - 1] = wStartowy;
    v[ilosc_wierzcholkow - 1] = graph->G[k][wStartowy];
    int suma=0;
    std::cout<<"Droga: "<<droga[0];
    for (int i = 0; i < graph->getV(); i++) {
        std::cout <<"->" << wk[i];
        suma = suma + v[i];
    }
    std::cout <<std:: endl;
    std::cout << "Koszt drogi: " << suma<<std::endl;
    auto t2 = Clock::now();
    double time = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();
    cout << "Czas wykonania: " << time << endl;

}